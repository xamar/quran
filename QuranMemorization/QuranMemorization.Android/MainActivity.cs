﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using Acr.UserDialogs;
using Android;

namespace QuranMemorization.Droid
{
    [Activity(Label = "QuranMemorization", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            UserDialogs.Init(this);

            string folderPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string exportFolderPath = folderPath.Replace("/com.companyname.QuranMemorization/files", "/");
            //string exportFolderPath = Android.OS.Environment.DirectoryDownloads;
            string dbFilePath = Path.Combine(folderPath, "memorized.db");
            string exportJsonFilePath = Path.Combine(exportFolderPath, "export.json");
            CheckAppPermissions();
            LoadApplication(new App(dbFilePath, exportJsonFilePath));
        }

        public void CheckAppPermissions()
        {
            var permissions = new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage };
            RequestPermissions(permissions, 1);
            //if ((int)Build.VERSION.SdkInt < 23)
            //{
            //    return;
            //}
            //else
            //{
            //    var permissions = new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage };
            //    RequestPermissions(permissions, 1);
            //}
        }
    }
}


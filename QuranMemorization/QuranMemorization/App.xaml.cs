﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace QuranMemorization
{
	public partial class App : Application
	{
        public static string DBlocation = string.Empty;
        public static string ExportLocation = string.Empty;

		public App ()
		{
			InitializeComponent();

			MainPage = new QuranMemorization.MainPage();
		}


        public App(string dbLocation, string exportLocation)
        {
            DBlocation = dbLocation;
            ExportLocation = exportLocation;

            InitializeComponent();

            MainPage = new NavigationPage(new QuranMemorization.MainPage());
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

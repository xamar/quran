﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace QuranMemorization.Model
{
    public class Surah
    {
        public string place { get; set; }
        public string type { get; set; }
        public int count { get; set; }
        public string title { get; set; }
        public string index { get; set; }
        public string pages { get; set; }
        public Juz[] juz { get; set; }
    }

    public class Juz
    {
        public string index { get; set; }
        public Verse verse { get; set; }
    }

    public class Verse
    {
        public string start { get; set; }
        public string end { get; set; }
    }
}

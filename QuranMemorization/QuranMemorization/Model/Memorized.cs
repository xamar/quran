﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuranMemorization.Model
{
    public class Memorized
    {
        private string title;

        [PrimaryKey]
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private int count;

        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        private string index;

        public string Index
        {
            get { return index; }
            set { index = value; }
        }

        private DateTime memorizedDate;

        public DateTime MemorizedDate
        {
            get { return memorizedDate; }
            set { memorizedDate = value; }
        }

        private DateTime lastRevisedDate;

        public DateTime LastRevisedDate
        {
            get { return lastRevisedDate; }
            set { lastRevisedDate = value; }
        }

    }
}

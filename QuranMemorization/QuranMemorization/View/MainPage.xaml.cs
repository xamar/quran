﻿using QuranMemorization.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuranMemorization
{
	public partial class MainPage : TabbedPage
	{
		public MainPage()
		{
			InitializeComponent();
        }

        public void Move_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage.Navigation.PushAsync(new SurahListPage());
        }
    }
}

﻿using QuranMemorization.Model;
using QuranMemorization.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Acr.UserDialogs;
using Newtonsoft.Json;
using System.IO;

namespace QuranMemorization.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SurahListPage : ContentPage
	{
		public SurahListPage ()
		{
			InitializeComponent ();

            var viewModel = new SurahListVM();
            surahsListView.ItemsSource = viewModel.surahs;
            surahsListView.ItemSelected += SurahsListView_ItemSelected;
		}

        private void SurahsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Surah surah = (Surah)e.SelectedItem;
            Memorized memSurah = new Memorized { Title = surah.title, Index = surah.index, Count = surah.count };            
            using (SQLiteConnection conn = new SQLiteConnection(App.DBlocation))
            {
                conn.CreateTable<Memorized>();
                var surahs = conn.Table<Memorized>().ToList();
                List<string> names = surahs.Select(s => s.Title).ToList();
                if (!names.Contains(memSurah.Title))
                {
                    memSurah.MemorizedDate = DateTime.Now;
                    conn.Insert(memSurah);
                    UserDialogs.Instance.Toast($"May Allah reward you for memorizing Surat {surah.title}. " +
                        $"Gaurd it by revising at Spaced Intervals.");
                }
                else
                {
                    memSurah.MemorizedDate = surahs.Where(s => s.Title == memSurah.Title).FirstOrDefault().MemorizedDate;
                    memSurah.LastRevisedDate = DateTime.Now;
                    conn.Update(memSurah);
                    UserDialogs.Instance.Toast($"May Allah reward you for revising Surat {surah.title}. " +
                        $"Keep revising at Spaced Intervals");
                }
            }
        }

        public void Export_Clicked(object sender, EventArgs e)
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DBlocation))
            {
                conn.CreateTable<Memorized>();
                var surahs = conn.Table<Memorized>().ToList();
                var json = JsonConvert.SerializeObject(surahs);
                StreamWriter writer = new StreamWriter(App.ExportLocation);
                writer.Write(json);
            }
        }

       
    }
}
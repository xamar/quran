﻿using QuranMemorization.Model;
using QuranMemorization.ViewModel;
using SQLite;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuranMemorization.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MemorizedPage : ContentPage
	{
		public MemorizedPage ()
		{
			InitializeComponent ();
        }

        protected override void OnAppearing()
        {
            try
            {
                var viewModel = new MemorizedVM();
                if (viewModel.surahs.Count == 0)
                {
                    StackLayout stackLayout = new StackLayout();
                    Content = stackLayout;
                    stackLayout.VerticalOptions = LayoutOptions.CenterAndExpand;
                    Label message = new Label
                    {
                        Text = "Please add the surahs you've memorized " +
                        "from the surah list. Tap on any surah to add here."
                    };
                    Button move = new Button { Text = "Touch Here to see surah list" };
                    stackLayout.Children.Add(message);
                    stackLayout.Children.Add(move);
                    move.Clicked += Move_Clicked;
                }
                else
                {
                    var memSurahs = viewModel.surahs.OrderByDescending(s => s.MemorizedDate).ToList();
                    memorizedSurahsListView.ItemsSource = memSurahs;
                }
            }
            catch (Exception exception)
            {
                App.Current.MainPage.DisplayAlert("Exception Occured", exception.Message, "Ok");
            }
        }

        public void Move_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage.Navigation.PushAsync(new SurahListPage());
        }
    }
}
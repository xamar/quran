﻿using QuranMemorization.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuranMemorization.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RevisionPage : ContentPage
	{
		public RevisionPage ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            var viewModel = new MemorizedVM();
            if (viewModel.surahs.Count == 0)
            {
                StackLayout stackLayout = new StackLayout();
                Content = stackLayout;
                stackLayout.VerticalOptions = LayoutOptions.CenterAndExpand;
                Label message = new Label
                {
                    Text = "Please add the surahs you've memorized " +
                    "from the surah list. Tap on any surah to add here."
                };
                Button move = new Button { Text = "Touch Here to see surah list" };
                stackLayout.Children.Add(message);
                stackLayout.Children.Add(move);
                move.Clicked += Move_Clicked;
            }
            else
            {
                var revSurahs = viewModel.surahs.OrderBy(s => s.LastRevisedDate).ToList();
                revisionSurahsListView.ItemsSource = revSurahs;
            }                
        }

        public void Move_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage.Navigation.PushAsync(new SurahListPage());
        }
    }
}
﻿using QuranMemorization.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuranMemorization.ViewModel
{
    public class MemorizedVM
    {
        public List<Memorized> surahs = new List<Memorized>();
        public MemorizedVM()
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(App.DBlocation))
                {
                    conn.CreateTable<Memorized>();
                    surahs = conn.Table<Memorized>().ToList();
                }
            }
            catch (Exception exception)
            {
                App.Current.MainPage.DisplayAlert("Exception Occured", exception.Message, "Ok");
            }
        }    
    }
}

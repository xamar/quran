﻿using Newtonsoft.Json;
using QuranMemorization.Model;
using QuranMemorization.View;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace QuranMemorization.ViewModel
{
    public class SurahListVM
    {
        public List<Surah> surahs = new List<Surah>();
        public SurahListVM()
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(SurahListPage)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("QuranMemorization.JsonResources.Surah.json");            
            using (StreamReader reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();
                surahs = JsonConvert.DeserializeObject<List<Surah>>(json);
            }
            surahs.Reverse();
        }
    }
}
